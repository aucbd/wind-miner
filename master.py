from datetime import datetime, timedelta
from json import dumps as json_dumps, load as json_load
from os import makedirs, path
from string import ascii_uppercase
from sys import argv
from typing import Any, Dict, List, Tuple, Union

import matplotlib.pyplot as plt
import requests

Number = Union[float, int]
TypeData = List[Tuple[Number]]
TypeDay = Dict[Union[str, int], Union[Number, List[Number]]]
TypeResults = Dict[str, Union[Number, List[Number], Dict[str, TypeDay]]]
TypePlot = Any


def data_get(date_beg: datetime, date_end: datetime, location: str, fields: List[int]) -> List[Tuple[Number]]:
    """
    Get data in the range [date_beg, date_end), return list of datapoints with fields.
    Always inserts time as first element of the datapoint.

    :param date_beg:
    :param date_end:
    :param location:
    :param fields:
    :raises HTTPError: on request error
    :return: List[Tuple[Number]]
    """

    data_raw: List[List[List[Number]]] = []
    data: TypeData

    base_url: str = "https://www.emd.dk/el/fetch2.php"
    interval: timedelta = timedelta(days=60)
    date_cur: datetime = date_beg

    while date_cur < date_end:
        params: dict = {
            "location": location.upper(),
            "start": date_cur.strftime("%Y-%m-%d"),
            "numberOfDays": interval.days,
        }

        res: requests.Response = requests.get(base_url, params=params)

        res.raise_for_status()

        data_raw += new_data if (new_data := res.json()) else []

        date_cur += interval

    fields.remove(7) if 7 in fields else None
    fields.insert(0, 7)

    data_raw = [
        [day[i] for i in fields]
        for day in data_raw
        if isinstance(day, list) and all(isinstance(d, list) for d in day)
    ]

    data = [
        datapoint
        for day in data_raw
        for datapoint in zip(*day)
    ]
    data.sort(key=lambda x: x[0])

    while datetime.fromtimestamp(data[-1][0]) >= date_end:
        del data[-1]

    return data


def data_filter(data: TypeData) -> TypeData:
    data = [
        datapoint
        for datapoint in data
        if all(
            0 < d < 7000
            for i, d in enumerate(datapoint[1:])
        )
    ]

    return data


def data_analyse(data: TypeData, day_beg: int, day_end: int, intervals: List[int]) -> TypeResults:
    intervals.sort()
    results: TypeResults = {
        "data_resolution": sum(abs(x[0] - y[0]) for x, y in zip(data[:-1], data[1:])) / (len(data) - 1) if data else 0,
        "intervals": intervals,
        "days": {},
    }

    data_dt: List[List[datetime, Number]] = [[datetime.fromtimestamp(dp[0]), *dp[1:]] for dp in data]
    data_days: Dict[str, List[List[datetime, Number]]] = {}

    # Group into days
    for dp in data_dt:
        dp_day = dp[0].strftime("%Y-%m-%d")
        data_days[dp_day] = data_days.get(dp_day, []) + [dp]

    del data_dt

    # Analyse each day
    for day, day_data in data_days.items():
        results["days"][day]: TypeDay = {
            "timestamp": 0,
            "time_above_cons": 0,
            **{k: [] for k in intervals}
        }

        period_beg: int = 0
        period_end: int = 0
        period_time: int

        for i, dp in enumerate(day_data):
            dp_time_f = (dp[0].hour * 3600 + dp[0].minute * 60 + dp[0].second) / 86400
            if dp_time_f * 24 < day_beg or dp_time_f * 24 > day_end:
                continue
            if not results["days"][day]["timestamp"]:
                results["days"][day]["timestamp"] = int(dp[0].timestamp())

            if dp[1] > dp[2]:
                period_beg = int(dp[0].timestamp()) if not period_beg else period_beg
                period_end = int(dp[0].timestamp())
            if period_beg and (dp[1] < dp[2] or i == len(day_data) - 1):
                period_time = period_end - period_beg
                period_time += round(results["data_resolution"])

                results["days"][day]["time_above_cons"] += period_time

                for interval in intervals[::-1]:
                    if period_time >= interval:
                        results["days"][day][interval].append(period_time)
                        break

                period_beg = period_end = 0

    return results


def data_plot(data: TypeData):
    data_unpacked: TypeData = list(zip(*data))
    data_times: List[datetime] = list(map(datetime.fromtimestamp, data_unpacked[0]))
    data_differences: List[Number] = list(map(lambda x: x[0] - x[1], zip(data_unpacked[1], data_unpacked[2])))

    grid = plt.GridSpec(2, 1)

    plt.xlabel("Time")

    plot1: TypePlot = plt.subplot(grid[0, 0])
    plot1.set(ylabel="MW")
    plot1.plot(data_times, data_unpacked[1], color="blue", label="Wind")
    plot1.plot(data_times, data_unpacked[2], color="red", label="Consumption")
    plot1.legend(prop={"size": 6})

    plot2: TypePlot = plt.subplot(grid[1, 0])
    plot2.set(ylabel="MW")
    plot2.plot(data_times, data_differences, color="purple", label="Wind - Cons.")
    plot2.axhline(0, linestyle=":", color="black")
    plot2.legend(prop={"size": 6})

    plt.show()


def results_plot(results: TypeResults):
    results_times: List[datetime] = [
        datetime.strptime(day, "%Y-%m-%d")
        for day in results["days"]
    ]
    results_intervals: List[List[List[Number]]] = [
        [results["days"][day][i] for i in results["intervals"]]
        for day in results["days"]

    ]

    for i, interval in enumerate(results["intervals"]):
        plt.bar(
            x=results_times,
            height=[
                sum(day_intervals[i])
                for day_intervals in results_intervals
            ],
            bottom=[
                sum([sum(day_i) for day_i in day_intervals[:i]])
                for day_intervals in results_intervals
            ],
            label=f"{interval/60:.0f} min"
        )

    plt.legend(prop={"size": 6})
    plt.show()


def object_2_json(data, dest: str):
    makedirs(path.dirname(dest), exist_ok=True)

    with open(dest, "w") as json:
        json.write(json_dumps(data))


def json_2_object(file: str) -> Union[TypeData, TypeResults]:
    data: TypeData

    with open(file, "r") as json:
        data = json_load(json)

    return data


def results_2_csv(results: TypeResults, dest: str):
    makedirs(path.dirname(dest), exist_ok=True)

    with open(dest, "w") as csv:
        csv.write(f'"DATE";"PERIODS";"TIME ABOVE CONSUMPTION"')
        csv.write(";" + ";".join([f"{i} sec TOT" for i in results["intervals"]]))
        csv.write(";" + ";".join([f"{i} sec NUM" for i in results["intervals"]]))
        csv.write("\n")

        for day, day_res in results["days"].items():
            csv.write(f'"{day}"')
            csv.write(f';{sum((len(day_res[i]) for i in results["intervals"]))}')
            csv.write(f";{day_res['time_above_cons']}")
            csv.write(";" + ";".join([str(sum(day_res[i])) for i in results["intervals"]]))
            csv.write(";" + ";".join([str(len(day_res[i])) for i in results["intervals"]]))
            csv.write("\n")

        csv.write(f';;;{";" * (len(results["intervals"]) * 2 - 1)}\n')
        for col in ascii_uppercase[1:3 + len(results["intervals"]) * 2]:
            csv.write(f";=SUM({col}2:{col}{len(results['days']) + 1})")


if __name__ == '__main__':
    wind_cons_data: TypeData
    wind_cons_results: TypeResults
    results_dir: str = path.join("results", argv[1])

    print("Getting data")
    if path.isfile(file_path := path.join(results_dir, "data.json")):
        wind_cons_data = json_2_object(file_path)
    else:
        wind_cons_data = data_get(
            datetime(int(argv[1]), 1, 1, 0, 0),
            datetime(int(argv[1]) + 1, 1, 1, 0, 0),
            "COMBINED",
            [2, 3]
        )
        object_2_json(wind_cons_data, path.join(file_path))

    print("Filtering", end="", flush=True)
    l_old: int = len(wind_cons_data)
    wind_cons_data = data_filter(wind_cons_data)
    print(f" {l_old-len(wind_cons_data)}/{l_old}")

    print("Analysing")
    wind_cons_results = data_analyse(wind_cons_data, int(argv[2]), int(argv[3]), [int(arg) for arg in argv[4:]])
    object_2_json(wind_cons_results, path.join(results_dir, "results.json"))

    results_2_csv(wind_cons_results, path.join(results_dir, f"results.csv"))

    print("Graph")
    data_plot(wind_cons_data)
    results_plot(wind_cons_results)

    print("Complete")
